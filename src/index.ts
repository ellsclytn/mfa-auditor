import glob from 'glob'
import parseDomain from 'parse-domain'
import yaml from 'js-yaml'
import { exec } from 'child_process'
import { promises } from 'fs'

const { readFile } = promises

type YamlBool = 'Yes' | 'No'

/** MFA Website candidate, parsed from YAML */
interface Website {
  name: string
  url: string
  tfa: YamlBool
  software?: YamlBool
  hardware?: YamlBool
}

/* Parsed YAML data */
interface YamlData {
  websites?: Website[]
}

/* Bitwarden's URI structure, for some reason */
interface BwUri {
  uri: string
}

/* Bitwaden service information */
interface RawBwItem {
  name: string
  login?: {
    uris?: BwUri[];
  }
}

/* Bitwarden service information, where valid login and URI data is present */
interface BwItem extends RawBwItem {
  login: {
    uris: BwUri[];
  }
}

/** Get Bitwarden services via Bitwarden CLI */
const getOwnServices = (): Promise<RawBwItem[]> =>
  new Promise((resolve, reject) => {
    exec('bw list items', (error, stdout, stderr) => {
      if (error) {
        console.error(stderr)
        return reject(error)
      }

      return resolve(JSON.parse(stdout))
    })
  })

const removeUselessBwItems = (items: RawBwItem[]): BwItem[] =>
  items.filter(item => item.login && item.login.uris) as BwItem[]

/** Details of a Service present in Bitwarden */
interface Service {
  name: string
  domain: string
}

/** Each BW Item can contain multiple URIs, so this function throws the service
 * name on each of them so we can later spread it onto a single-dimension
 * `services` array.
 */
const addServiceNameToUris = (name: string, uris: BwUri[]): Service[] =>
  uris
    .map(({ uri }) => {
      const uriInfo = parseDomain(uri)
      if (!uriInfo) {
        console.warn(`Unable to parse URI ${uri}`)
        return null
      }

      return {
        name,
        domain: `${uriInfo.domain}.${uriInfo.tld}`
      }
    })
    .filter(service => service) as Service[]

/** We effectively have to deal with a two dimsional array: the first dimension
 * is the array of services. The second dimension is the URIs that a service is
 * known to have. The reduce simply flattens the array into a list of
 * service-named domains.
 */
const normalizeBwItems = (items: BwItem[]): Service[] =>
  items.reduce(
    (acc: Service[], item) => [
      ...acc,
      ...addServiceNameToUris(item.name, item.login.uris)
    ],
    []
  )

const YamlBoolToBool = (condition?: YamlBool): boolean => condition === 'Yes'

const getFilePaths = (): Promise<string[]> =>
  new Promise((resolve, reject) => {
    glob('../twofactorauth/_data/**/*.yml', (error, files) => {
      if (error) return reject(error)

      resolve(files)
    })
  })

const readFileToJson = (path: string): Promise<YamlData> =>
  readFile(path, 'utf-8').then(yaml.safeLoad)

const filterMfaSitesOnly = (websites: Website[]) =>
  websites.filter(
    website =>
      YamlBoolToBool(website.hardware) || YamlBoolToBool(website.software)
  )

interface MfaData {
  name: string
  software: boolean
  hardware: boolean
  domain: string
}

const websiteToMfaData = (website: Website) => {
  const uriInfo = parseDomain(website.url)
  if (!uriInfo) throw new Error(`Unable to parse URI ${website.url}`)

  return {
    name: website.name,
    hardware: YamlBoolToBool(website.hardware),
    software: YamlBoolToBool(website.software),
    domain: `${uriInfo.domain}.${uriInfo.tld}`
  }
}

const normalizeSiteData = (siteData: YamlData[]): MfaData[] =>
  siteData.reduce(
    (acc: MfaData[], siteSet) => [
      ...acc,
      ...(siteSet.websites
        ? filterMfaSitesOnly(siteSet.websites).map(websiteToMfaData)
        : [])
    ],
    []
  )

const checkForMatchingServices = ([mfaSiteData, bwServices]: [
  MfaData[],
  Service[]
]) => {
  const confirmedMfaServices = bwServices.filter(service => {
    const match = mfaSiteData.find(
      mfaService => mfaService.domain === service.domain
    )

    if (match) {
      if (match.software && match.hardware) {
        console.log(`${match.name} offers software and hardware MFA`)
      }

      if (match.software && !match.hardware) {
        console.log(`${match.name} offers software MFA.`)
      }

      if (!match.software && match.hardware) {
        console.log(`${match.name} offers hardware MFA.`)
      }
      return true
    }
  })

  console.log(
    `\n  ${confirmedMfaServices.length} services you use offering MFA.`
  )
}

const bwServices = getOwnServices()
  .then(removeUselessBwItems)
  .then(normalizeBwItems)

getFilePaths()
  .then(paths => Promise.all(paths.map(readFileToJson)))
  .then(normalizeSiteData)
  .then(mfaSiteData => Promise.all([mfaSiteData, bwServices]))
  .then(checkForMatchingServices)
  .catch(console.error)
