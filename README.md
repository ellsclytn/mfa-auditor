# MFA Auditor

**A thing to tell you what services in your Bitwarden offer Multi-Factor Authentication**

---

## Prerequisites

* [Bitwarden CLI](https://github.com/bitwarden/cli)
* [Node](https://nodejs.org/en/)

## Usage

1. Clone https://github.com/2factorauth/twofactorauth without renaming the cloned folder.
2. Clone this repo at the same level.
3. Log in to Bitwarden CLI (`bw login`) and set the session env var.
4. `yarn install`
5. `yarn start`
